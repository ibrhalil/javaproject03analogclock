package gui;

import javax.swing.*;
import java.awt.*;

public class Pencere extends JFrame
{
    JPanel saatPanel;
    public Pencere()
    {
        this.setMinimumSize(new Dimension(480, 360));
        setLocation(400,200);
        setTitle("Analog Saat");

        saatPanel = new Saat();
        setBackground(new Color(255, 255, 255));

        add(saatPanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
