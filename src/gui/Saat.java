package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

enum SaatCubukCesit
{
    AKREP,YELKOVAN,SANİYE;
}

class SaatCubuk
{
    private int x,y,yukseklik,kalinlik;
    private double aci;
    private Color color;

    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public int getYukseklik()
    {
        return yukseklik;
    }

    public void setYukseklik(int yukseklik)
    {
        this.yukseklik = yukseklik;
    }

    public int getKalinlik()
    {
        return kalinlik;
    }

    public void setKalinlik(int kalinlik)
    {
        this.kalinlik = kalinlik;
    }

    public double getAci()
    {
        return aci;
    }

    public void setAci(double aci)
    {
        this.aci = aci;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }
}

public class Saat extends JPanel implements ActionListener
{
    private int saatYariCap;
    private int kalinlik = 40;
    private int merkezX;
    private int merkezY;
    private Timer zaman;

    public Saat()
    {
        zaman = new Timer(999,this);
        setBackground(new Color(255, 255, 255));
        setVisible(true);
        zaman.start();
    }


    @Override protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        merkezX = getWidth()/2;
        merkezY = getHeight()/2;

        saatYariCap = getHeight()<getWidth() ? this.getHeight()*60/100:this.getWidth()*60/100;

        /*Saat çerçeve*/
        ((Graphics2D) g).setStroke(new BasicStroke(saatYariCap/kalinlik));

        //g.setColor(new Color(255, 255, 255));
        //g.fillOval(merkezX-(saatYariCap/2), merkezY-(saatYariCap/2), saatYariCap, saatYariCap);

        g.setColor(new Color(0, 0, 0));
        g.drawOval(merkezX-(saatYariCap/2), merkezY-(saatYariCap/2), saatYariCap, saatYariCap);
        g.fillOval(merkezX-(saatYariCap/(kalinlik*2)), merkezY-(saatYariCap/(kalinlik*2)), saatYariCap/kalinlik, saatYariCap/kalinlik);

        /*Saat dakika cizgileri*/
        for(int i = 0; i<60; i++)
        {
            double kO = i%5==0?1.11:1.07;
            ((Graphics2D) g).setStroke(new BasicStroke(i%5==0?5:1));
            int uzunluk = saatYariCap/2;
            double aci=Math.toRadians(i*6);
            aci-=Math.toRadians(15)*6;
            int x = (int)(uzunluk/kO * Math.cos(aci));
            int y = (int)(uzunluk/kO * Math.sin(aci));
            g.drawLine(merkezX+(int)(x*kO), merkezY+(int)(y*kO),merkezX+x, merkezY+y);
            //g.drawLine(merkezX+x, merkezY+y,merkezX , merkezY);
        }

        /*Saat rakamları*/
        int sK4 = saatYariCap/kalinlik*4;
        g.setFont(new Font(null, Font.BOLD, sK4));
        g.drawString("3",merkezX+(saatYariCap*37/100),merkezY+(int)(sK4/2.5));
        g.drawString("6",merkezX-(int)(sK4/3.5),merkezY+(saatYariCap*43/100));
        g.drawString("9",merkezX-(saatYariCap*42/100),merkezY+(int)(sK4/2.5));
        g.drawString("12",merkezX-2*(int)(sK4/3.5),merkezY-(saatYariCap*35/100));
        /**/

        //Test
        /*
        g.setColor(new Color(255, 0, 0));
        ((Graphics2D) g).setStroke(new BasicStroke(1));

        g.drawLine(getWidth()/2,0,getWidth()/2,getHeight());
        g.drawLine(0,getHeight()/2,getWidth(),getHeight()/2);
        */
        hesap(g,SaatCubukCesit.AKREP);
        hesap(g,SaatCubukCesit.YELKOVAN);
        hesap(g,SaatCubukCesit.SANİYE);
    }

    private void hesap(Graphics g, SaatCubukCesit cesit)
    {
        SaatCubuk cubuk = new SaatCubuk();
        switch(cesit){
            case AKREP:
                cubuk.setKalinlik(saatYariCap/kalinlik/2);
                cubuk.setYukseklik(saatYariCap*2/10);
                cubuk.setColor(new Color(0, 0, 0));
                cubuk.setAci(Math.toRadians((LocalTime.now().getHour())*30));
                cubuk.setAci(cubuk.getAci()-Math.toRadians(3)*30);
                break;
            case YELKOVAN:
                cubuk.setKalinlik(saatYariCap/kalinlik/2);
                cubuk.setYukseklik(saatYariCap*3/10);
                cubuk.setColor(new Color(0, 0, 0));
                cubuk.setAci(Math.toRadians((LocalTime.now().getMinute())*6));
                cubuk.setAci(cubuk.getAci()-Math.toRadians(15)*6);
                break;
            case SANİYE:
                cubuk.setKalinlik(saatYariCap/kalinlik/4);
                cubuk.setYukseklik(saatYariCap*4/10);
                cubuk.setColor(new Color(255, 0, 0));
                cubuk.setAci(Math.toRadians((LocalTime.now().getSecond())*6));
                cubuk.setAci(cubuk.getAci()-Math.toRadians(15)*6);
                break;
        }

        ((Graphics2D) g).setStroke(new BasicStroke(cubuk.getKalinlik()));
        g.setColor(cubuk.getColor());

        int x = (int)(cubuk.getYukseklik() * Math.cos(cubuk.getAci()));
        int y = (int)(cubuk.getYukseklik() * Math.sin(cubuk.getAci()));

        cubuk.setX(x);
        cubuk.setY(y);
        g.drawLine(merkezX, merkezY,merkezX+cubuk.getX() , merkezY+ cubuk.getY());
    }

    @Override public void actionPerformed(ActionEvent e)
    {
        repaint();
    }
}
